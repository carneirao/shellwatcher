requires "Dancer2"        => "0.159002";
requires "MooseX::Traits" => "0.12";
requires "RRD::Simple"    => "1.44";
requires "Log::Log4perl"  => "1.46";
requires "Class::Accessor" => "0.34";
requires "UUID::Object"    => "0.81";
requires "UUID::Generator::PurePerl" => "0.80";
requires "common::sense"   => "3.73";
requires "Net::MessageBus" => "0.08";
requires 'ExtUtils::MakeMaker' => '6.17';
requires "JSON" => '2.90';
requires "Dancer2::Plugin::Auth::Extensible" => "0.306";

recommends "YAML"             => "0";
recommends "URL::Encode::XS"  => "0";
recommends "CGI::Deurl::XS"   => "0";
recommends "HTTP::Parser::XS" => "0";

on "test" => sub {
    requires "Test::More"            => "0.88";
    requires "HTTP::Request::Common" => "0";
};
