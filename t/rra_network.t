
use Test::More;
use Test::Exception;

use FindBin qw($Bin);
use lib "$Bin/lib";

my $host = 'testing';
my $cache_dir = $Bin;
$cache_dir =~ s#/t#/cache/$host#;


use_ok('ShellWatch::RRA');

my $metric = ShellWatch::RRA->new_with_traits(
    traits => ['network'],
    host   => 'testing',
    time   => time,
    dev    => 'eth0',
    tx_bytes => 12345,
    rx_bytes => 123,
);

ok( $metric->does('ShellWatch::RRA::network'), 'Instance' );

ok( -e join('/',$cache_dir,'memory.rrd'),'check_rrd_file_network' );

ok( $metric->update, 'update' );

ok( $metric->dev('eth2'), 'check rw ->dev');
like($metric->rrd_file, qr/eth2/, 'check rrd_file');

ok( $metric->txbytes(555), 'check rw ->txbytes');
ok( $metric->rxbytes(222), 'check rw ->rxbytes');

dies_ok( sub { $metric->txbytes('xxx') } , 'check isa for txbytes');
dies_ok( sub { $metric->rxbytes('yyy') } , 'check isa for rxbytes');

ok( $metric->graph, 'graph' );

done_testing();

