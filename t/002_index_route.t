use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/lib","$Bin";

use Agent qw/get_status $cookie_file/;
use shellwatch;
use Test::More;
use Plack::Test;
use HTTP::Request::Common;
use Sys::Hostname; #core module
use Data::Dumper;


$cookie_file = '/tmp/shellwatch-tester.cookie';
my $hostname = hostname;

my $url_base = 'http://' . $hostname . ':' . '5000';

my $app = shellwatch->to_app;
is( ref $app, 'CODE', 'Got app' );


#HOSTS tests
#/host without log-in 
my $url = $url_base .'/hosts' ;
my $r = get_status( 'GET', $url );
is( $r->{status_code}, 401, 'hosts_without_loggin' ); 

done_testing();
