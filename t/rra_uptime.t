
use Test::More;
use Test::Exception;

use FindBin qw($Bin);
use lib "$Bin/lib";

my $host = 'testing';
my $cache_dir = $Bin;
$cache_dir =~ s#/t#/cache/$host#;


use_ok('ShellWatch::RRA');

my $metric = ShellWatch::RRA->new_with_traits(
    traits => ['uptime'],
    host   => $host,
    time   => time,
    service  => 'ssh',
    up => 1,
);

ok( $metric->does('ShellWatch::RRA::uptime'), 'Instance' );

ok( -e join('/',$cache_dir,'memory.rrd'),'check_rrd_file_network' );

ok( $metric->up(1), 'up => 1');

ok( $metric->update, 'update' );

ok( $metric->service('ftp'), 'check rw ->service');
like($metric->rrd_file, qr/ftp/, 'check ftp_file');

ok( $metric->up(1), 'check up => 1');
ok( ! $metric->up(0), 'check up => 0');

dies_ok( sub { $metric->tx_bytes('xxx') } , 'check isa for up');
dies_ok( sub { $metric->rx_bytes('123') } , 'check isa for up');

ok( $metric->graph, 'graph' );

done_testing();

