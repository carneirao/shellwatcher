package Agent;
require Exporter;
  @ISA = qw(Exporter);
  @EXPORT_OK = qw(get_status login $cookie_file);
use common::sense;
use Data::Dumper;
our $cookie_file = '';
our $CURL_CMD = '/usr/bin/curl -L -I  ';
$CURL_CMD .= "-b $cookie_file" if defined($cookie_file) and $cookie_file;
$CURL_CMD .= ' -X ';

sub get_status {
  my(@options) = @_;
  my $cmd = join ' ' , $CURL_CMD, @options;
  my $Code = {};
  my $resp = '';
  foreach(@{run_command($cmd)}) {
    if($_ =~ m{(HTTP.*?)/([\d\.]+).+?(\d+).+?([\w\s]+)} ) {
      my ($protocol, $version, $status_code, $status_msg) = ($1,$2,$3,$4); 
      $status_msg =~ s/\n|\r//g;
      next if $status_code >= 300 and $status_code < 400;     
      $Code = { status_code => $status_code,
                status_msg => $status_msg,
              };
    }
    else {
      next;
    }
  }
  return $Code;
}


sub login {
  my ($login_url) = @_;
  my $CMD = "curl -c $cookie_file -X POST $login_url";

}

sub run_command {
  my $cmd = shift;
  my @Raw_response = readpipe $cmd;
  return \@Raw_response;
}

1;
