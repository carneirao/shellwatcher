package shellwatch;

use common::sense;
use Dancer2;
use FindBin qw/$Bin/;
use lib './lib';
use lib "$Bin/../lib";
use Dancer2::Plugin::Auth::Extensible;
use JSON qw/encode_json decode_json/;
use ShellWatch::RRA;
use ShellWatch::ScriptMeta;
use ShellWatch::Util qw(sw_dir);
use ShellWatch::API; #It's how 
use File::Path qw/make_path/;
use Log::Log4perl;
use Data::Dumper;
use DateTime;

prefix undef;

#Globals
our $VERSION = '0.3.0';
our $metrics;
our $stash = {};

#setting 'default' as default theme :p

our $logger = Log::Log4perl->get_logger(config->{appname});
our $user;

#Init log
Log::Log4perl->init( config->{logfile} );

# cache objects to preserve sane memory.
sub _get_metric {
  my ( $host, $type ) = @_;
  my ( $class, $extra );
  if ( $type =~ /-/ ) {
    ( $class, $extra ) = split '-', $type;
  }
  else {
    ( $class, $extra ) = ( $type, '' );
  }
  eval {
    if ( !$metrics->{$host}->{$type} ) {
      $metrics->{$host}->{$type} = ShellWatch::RRA->new_with_traits(
        traits => [$class],
        %{ params() },
        logger => $logger,
      );
    }
    foreach my $key ( keys %{ params() } ) {
      $metrics->{$host}->{$type}->{$key} = params->{$key};
    }
  };
  $logger->warn( $@ ) and send_error( 'Not enough parameters', 400 ) if $@;
  return $metrics->{$host}->{$type};
}

# count files in a directory
sub _files_in_dir {
  my $dir   = shift;
  my $total = 0;
  opendir my ($mh), $dir or ( $logger->error("Can't open dir '$dir' - $!") and die $! );
  while ( my $file = readdir $mh ) {
    $total++ unless $file =~ /^\./;
  }
  closedir($mh);
  return $total;
}

#get available metric vars from RRD
sub _get_metrics_vars {
  my ( $host, $user, $allmetrics, $extra ) = @_;
  my $vars;

  if( !defined($host) || !$host ){
    $logger->error('host parameter is missing!');
  }
  elsif( !defined($user) || !$user ){
    $logger->error('user is not logged!');
    redirect '/login';
  }
  elsif( !@{$allmetrics} ) {
    $logger->error('missing metric data!');
  }
  else {
    my @Names = ();
    map { push @Names, $_->{name}; } @{$allmetrics};
    foreach my $name (@Names) {
      my $class = $name;
      $class =~ s/-.*$// if $class =~ /-/;
      my $metric = ShellWatch::RRA->new_with_traits(
        traits => [$class],
        host   => $host,
        logger => $logger,
      );
     $vars->{$name} = $metric->vars;
      undef $metric;
    }
  }
  return $vars;
}

#get the hooks. #TODO change name from ScriptMeta to Hooks
sub _get_hooks {
  my ($host,$user) = @_;
  my $meta = ShellWatch::ScriptMeta->new( host => $host,
                                          logger => $logger,
                                          user => $user,
  );
  return $meta->tree_json;
}

#bring a Perl structure with all metadata from hooks.
sub _get_hooks_tree {
  my ($host,$user) = @_;
  my $meta = ShellWatch::ScriptMeta->new( host => $host, 
                                          logger => $logger,
                                          user => $user,
  );
  return $meta->tree;
}

#gets theme based on user roles. 
#Role name MUST have 'theme-' prefix
sub _theme {
  $user = logged_in_user;
  my $theme = 'default';
  my @Roles = @{$user->{roles}};
  foreach my $role(@Roles){ #themes will be overwriten if exists more than one
    if($role =~ /theme-(.*)$/){
      $theme = $1;
      $theme = 'default' if !defined($theme) or !$theme;
    }
  }
  #setting on 'stash'(vars)
  my $theme_dir = $Bin . '/..' . config->{theme_dir};
  `cp -r $theme_dir/default $theme_dir/$theme` if !-d join( '/', $theme_dir,$theme );
  var theme => $theme;
  var user => $user;
  session user => $user;
  return $theme;
}


sub stash {
  my($self,$stash) = @_; 
  $self->{stash} = $stash if defined($stash);
  return $stash;
}


#######
####### ROUTES


hook before_template_render => sub {
  my $theme = _theme();
  my $config_wrapper = join '/' , $theme, config->{'engines'}->{'template'}->{'template_toolkit'}->{WRAPPER};
  config->{engines}->{template}->{'template_toolkit'}->{WRAPPER} = $config_wrapper;

};



get '/' => sub {
  redirect '/hosts';
};


get '/about' => sub {
  template "about.tt";
};

get '/hosts' => require_login sub {
  _theme();
  $user = logged_in_user;
  my $hosts_dir = sw_dir('cache',);
  my @hosts;
  opendir my ($dh), $hosts_dir or send_error( 'Not found', 404 );
  while ( my $name_dir = readdir $dh ) {
    my $current_dir = join( '/', $hosts_dir, $name_dir );
    next unless -d $current_dir;
    next if $name_dir =~ /^\./ || $name_dir =~ /test/; #bypass on tests...
    push( @hosts,
      { host => $name_dir, 
        metrics => &_files_in_dir($current_dir) 
      } );
  }
  closedir($dh);

  #ordenando os hosts pelo nome
  my @sorted = sort { $a->{host} cmp $b->{host} } @hosts;
  template vars->{theme} . '/hosts.tt' => { hosts => [@sorted], 
                                            user => $user->{user},
                                          };
};

get '/host/:host' => require_login sub {
  _theme();
  $user = logged_in_user;
  my $host = params->{'host'} or send_error( 'No host', 400 );
  my $host_dir = &sw_dir( 'public', 'graph', param('host') );
  my @metrics;

  opendir my ($dh), $host_dir or send_error( 'Not found', 404 );
  while ( my $name_dir = readdir $dh ) {
    my $current_dir = join( '/', $host_dir, $name_dir );
    next unless -d $current_dir;
    next if $name_dir =~ /^\./;

    my $file = "$name_dir/$name_dir-daily.png";
    my $img = join( '/', '/graph', params->{'host'}, $file );
    push( @metrics, { name => $name_dir, image => $img } );
  }
  closedir($dh);
  my $hooks            = _get_hooks($host,$user->{user});
  my $hooks_tree       = _get_hooks_tree($host,$user->{user});
  my $metric_vars      = _get_metrics_vars( $host, $user->{user}, \@metrics, );
  my @Notifiers_levels = ( 'CRITICAL', 'WARNING', 'UNKNOWN' );
  $metric_vars = {} if !defined $metric_vars or !$metric_vars;
  template vars->{'theme'} . '/host.tt' => {
    notifiers_templates => ['email.tt'],    #TODO put this in some config file
    metrics             => [@metrics],
    metric_vars      => encode_json($metric_vars),
    hooks            => $hooks,
    hooks_tree       => $hooks_tree->{$host},
    Notifiers_levels => \@Notifiers_levels,
    user             => $user->{user},
  };
};

post '/host/:host/:type' => require_login sub {
  my $host = params->{'host'} or send_error( 'No host',       400 );
  my $time = params->{'time'} or send_error( 'No epoch time', 400 );
  my $type = params->{'type'} or send_error( 'No type',       400 );

  # extra is an argument to be use with "multi-metrics" modules.
  $type .= '-' . params->{'extra'} if params->{'extra'};
  my $metric = &_get_metric( $host, $type );
  $metric->update;

  #Reading headers and last values from RRD
  my $data = {
    host   => $host,
    metric => $type
  };
  my $rrd_data = $metric->get_RRD_Data($data);

  foreach my $var ( keys %{$rrd_data} ) {
    my $metric_dir = join '/', ( $metric->hook_directory, $type );

    if ( !-d $metric_dir ) {
      make_path($metric_dir);
    }
  }

  _register_metric_sent($host,$type);

  #Generates metric chart
  $metric->graph;
  return 1;
};


sub _register_metric_sent {
  my ($host,$type) = @_;
  my $ok = 0;
  my $filepath = join '/',$Bin,'../','cache',$host,'last_time_sent';
  my $fh = undef;
  if( open $fh,'>',$filepath ){
    my $dt = DateTime->now( time_zone => 'America/Sao_Paulo' ); #TODO recuperar timezone do config
    print $fh $dt->epoch;
    $ok = 1;
    undef $dt;
  }
  close $fh if defined $fh;
  return $ok;
}


get '/host/:host/:metric' => require_login sub {
  $user = logged_in_user;
  _theme();
  my $host   = params->{'host'}   or send_error( 'No host',   400 );
  my $metric = params->{'metric'} or send_error( 'No metric', 400 );

  my $metric_dir = &sw_dir( 'public', 'graph', param('host'), $metric );
  my @graphs;

  opendir my ($dh), $metric_dir or send_error( 'Not found', 404 );
  while ( my $file = readdir $dh ) {
    my $current_dir = join( '/', $metric_dir, $file );
    next unless -f $current_dir;
    next unless $file =~ /png$/;
    push( @graphs,
      join( '/', '/graph', params->{'host'}, params->{'metric'}, $file ) );
  }
  closedir($dh);

  my $meta = ShellWatch::ScriptMeta->new( host => $host, 
                                          logger => $logger,
                                          user => $user->{user},
  );

  my $obj = &_get_metric( $host, $metric );
  template
    vars->{'theme'} . '/graph.tt' => {
    description      => $obj->metric_description,
    graphs           => [@graphs],
    metric_tree_json => $meta,
    },
    ;
};


any '/do_logout' => require_login sub {
  context->destroy_session;
  redirect '/';
};



=pod

=encoding utf8

=head1 NAME

shellwatch.pm - Web app class for centralize UI interface routes.

=head1 DESCRIPTION

shellwatch.pm is the main class of a Dancer2 application to centralize routes that provides
user interface via HTML and JSON responses( See ROUTES session for more details ).
 

=head1 SYNOPSIS

 cd <SHELLWATCH_HOME> No, this variable doesn't exists. It's just to illustrate the reference to path where shellwatch is.
 plackup bin/app.psgi
 or
 starman <OPTIONS> bin/app.psgi
 or
 WHATEVER PSGI server/client you want...



=head1 ROUTES


=head2 get /

Redirects to /hosts

=head2 get /about

Provides UI for 'about' content

=head2 get /hosts

Provides UI for all hosts available in shellwatch 

=head2 get /host/:host

Provides UI for a particular host available in shellwatch

=head2 post /host/:host/:type

Send metric data to Shellwatch. 

Parameters:   

=over

=item * host: name of the client host
  
=item * time: epoch time of the moment that metric data was send
  
=item * type: type or name(same thing) of the metric

=item * extra: option value to send some detail of metric that is available, example: some device name

=back




=head2 get /host/:host/:metric

Provides UI for a particular metric


=head1 METHODS


=head2 _get_metric(Str $host, Str $type)
 
fskfhsl a fslkfhaf a alfhqpouopbuzbz.,nefpqh  apoajfa a [saofa[ofuqowur  a;fa;fj as

=head2 _files_in_dir(Str $dir)
 
asfkhfasklfasf asf;kashjf askfh asf ashf ka;shfa

=head2 _get_hooks(Str $host)

Try to get all hooks from some host. If successful, returns a HashRef with the 
tree for all scripts of this hosts. Otherwise returns a empty HashRef

=head2 _get_hooks_tree(Str $host)

Almost same thing as _get_hooks. The diference is the type of returning. Instead HashRef returns JSON.

=head1 CHANGING THEMES

You can set theme by user role. To do that, you need to change file config.yml. Ex.:

 plugins:
   Auth::Extensible:
     realms:
       config:
         provider: Config
         users:
           - user: 'admin'
             pass: '{SSHA}somepasswordencrypted'
             roles:
               - horrorscope
               - theme-mytheme

In 'roles' settings, you can alter themes just change the name of the role that starts with 'theme-' as prefix. 

An user can be added manually, but is recommended that you use bin/create_user script. Trye bin/create_user --help

After that, make a copy from default view: cp views/default views/mytheme

Now you can alter appearance from view/mytheme.



=head1 AUTHOR

Thiago Rondom L<thiago.rondom@eokoe.com>

=head1 MAINTAINER

Andre Carneiro L<andre.carneiro@eokoe.com>

=head1 LICENSE

Copyright 2015 - Eokoe


=cut

# ABSTRACT: turns baubles into trinkets


1;

