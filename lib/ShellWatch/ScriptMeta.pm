use common::sense;

package ShellWatch::ScriptMeta;
use Moose;
use namespace::autoclean;
use JSON;
use File::Path qw/make_path/;
use Data::Dumper;
use ShellWatch::Util qw/sw_dir/;
use FindBin qw/$Bin/;

our $VERSION = '0.1.6';

has host => (
  is  => 'rw',
  isa => 'Str',
  required => 1,
);

has base_dir => (
  is      => 'rw',
  isa     => 'Str',
  default => join '/', "$Bin/../",'meta',
);

has user => (
  is => 'rw',
  isa => 'Str',
  required => 1,
);

has tree => (
  is      => 'rw',
  default => sub {return {}}, 
);

has tree_json => (
  is  => 'rw',
  isa => 'Str',
);

has logger => (
  is => 'rw',
);


sub BUILD {
  my ( $self, ) = @_;
  my $return;
  my $tree = {};
  my $user = $self->user;
  if ( -d $self->base_dir ) {
    opendir my $dh, $self->base_dir or die $!;
    my $base = $self->base_dir;
    while ( my $host = readdir $dh ) {
      chomp $host;
      next if $host =~ s/^\.// or $host ne $self->host;
      if ($host eq $self->host ) {
        my $user_dir = join '/', $base, $host, $user;
        make_path $user_dir if !-d $user_dir;
        opendir my $dh2, $user_dir or die $!;
        while ( my $metric = readdir $dh2 ) {
          next if $metric =~ /^\./;
          next if $metric =~ /\..+?$/;
          my $dir = join '/', $user_dir, $metric;
          make_path $dir if !-d $dir;
          my $metric_dir = join '/', $base, $host, $user, $metric ;
          opendir my $dh3, $metric_dir or die $!;
          while ( my $script = readdir $dh3 ) {
            next if $script !~ /\.sh$/;
            my $script_file = join '/', $base, $host, $user, $metric, $script ;
            if( -e $script_file ){
              push @{ $tree->{$host}->{$metric}},{ 
                                                    identifier => $script,
                                                    bash_template => $self->_extract_code($script_file),
                                                    period => $self->_get_period($script_file), #test...
                                                 };
            }
          }
          close $dh3 if defined $dh3;
        }
        closedir $dh2 if defined $dh2;
      }
    }
    closedir $dh if defined $dh;
  }
  $self->tree($tree);
  $self->tree_json( encode_json( $self->tree ) )
  if defined $self->tree and ref( $self->tree );
  $return = $self->tree;
}



sub _extract_code {
  my ( $self, $file ) = @_;
  my $lines = '';
  if( !open my $fh, '<encoding(utf8)', $file ){
    $self->logger("Cant open file '$file' - $!") if defined $self->logger;
  }
  else {
    while (<$fh>) { $lines .= $_ }
    close $fh;
  }
  return $lines;
}



sub _get_period {
  my ( $self, $file ) = @_;
  my $lines = '';
  $file =~ s/\.sh$/.period/;
  if( !open my $fh, '<encoding(utf8)', $file ){
    $self->logger("Cant open file '$file' - $!") if defined $self->logger;
  }
  else {
    while (<$fh>) { $lines .= $_ }
    close $fh;
  }
  return $lines;
}



__PACKAGE__->meta->make_immutable;




=pod

=encoding utf-8

=head1 NAME

ShellWatch::ScriptMeta - Meta data and content control for scripts.

=head1 DESCRIPTION

This class has one go: acess tree meta information for script. This 'tree' is basically a hash
that contains all keys and values of all user's hooks.

=head1 SYNOPSIS

 use ShellWatch::ScriptMeta;
 use MyLogger; #some Log4perl compatible logger
 use Data::Dumper;
 my $meta = SHellWatch::ScriptMeta->new( host => 'somehost',
                                         user => 'someuser',
                                       
                                    ));
 #see the hash tree...
 say Dumper $meta->tree;
 
 #see the JSON tree...
 say $meta->tree_json;
                                  


=head1 ATTRIBUTES

=over

=item * base_dir - base dir for hooks

=item * host - some host address

=item * user - some fortunate user - :D

=item * tree - At the moment of instance of 'ShellWatch::ScriptMeta', 'tree' MUST have a hashref with all hooks from an user.

=item * tree_json - At the moment of instance of 'ShellWatch::ScriptMeta', 'tree_json' MUST have a JSON representation of tree.

=item * logger - Must be a 'Log::Log4perl' compatible handler. 

=back

=head1 INSTANCE

When it calls 'new' method(Moose), The class will try automatically populate tree and tree_json attributes with
all ShellWatch::Script object information and the script code.

=head1 INTERNALS

=head2 _extract_code($filepath)

Open the hook script file located at <PATH_TO_SHELLWATCH>/hooks/<METRIC>/<metric>.sh and return the lines.
If file doesn't exists or some error throws exception, the exception will be logged and the method 
will return a empty string('').

=cut


1;
