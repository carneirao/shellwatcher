use common::sense;
package ShellWatch::RRA;

use Moose;
with 'MooseX::Traits';

use FindBin qw($Bin);
use File::Path qw(make_path);
use RRD::Simple;
use Data::Dumper;

has 'host' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1
);

has 'api_time' => (
    is       => 'ro',
    isa      => 'Str', 
    required => 0,
    default  => time
);


has 'rrd_directory' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $dir = join( '/', $Bin, '..', 'cache', shift->host );
        make_path($dir) if ! -d $dir;
        return $dir;
    }
);

has 'graph_directory' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $dir =
          join( '/', "$Bin/../", 'public', 'graph', $self->host, $self->metric_name );
        make_path($dir) if ! -d $dir;
        return $dir;
    }
);

has 'hook_directory' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $dir =
          join( '/', $Bin, '..', 'meta', $self->host );
        make_path($dir) if ! -d $dir;
        return $dir;
    }
);


has rrd => (
    is      => 'ro',
    isa     => 'Object',
    lazy    => 1,
    default => sub {
        RRD::Simple->new( file => shift->rrd_file,
                          cf => ['MIN','MAX','AVERAGE'],
                          step => 5,
        );
    }
);

has '+_trait_namespace' => ( default => 'ShellWatch::RRA' );


has graph_width => (
    is      => 'ro',
    isa     => 'Int',
    default => sub {
        $ENV{GRAPH_WIDTH} || 760
    }
);

has graph_height => (
    is      => 'ro',
    isa     => 'Int',
    default => sub {
        $ENV{GRAPH_HEIGTH} || 150
    }
);


sub rrd_time { time }

sub get_RRD_Data {
  my ($self,$data) = @_;

  if(!defined($data) || !$data){
    die 'No information to get RRD data...';
  }
  else {
    my $ctime  = time;
    my $rrdres = 900;
    my ( $host, $metric_name ) = ( $data->{host}, $data->{metric} );
    my $rrd_dir = $self->rrd_directory;
    my $cmd = "rrdtool fetch $rrd_dir/$metric_name.rrd AVERAGE -r $rrdres -e @{[int($ctime/$rrdres)*$rrdres]} -s e-1h";
    my @RRD = readpipe $cmd;
    my $headers = shift @RRD;
    $headers =~ s/^\s+//;
    $headers =~ s/\s+$//;
    while($headers =~ /(\s\s|\s\s\s)+/){ $headers =~ s/(\s\s|\s\s\s)+/ /g };
    my @Headers = split /\s/,$headers;
    $data = {};
    foreach my $h(@Headers){
      my $attr = lc($h);
      $data->{$attr} = $self->$attr if defined $self->$attr;
    }
  }
  return $data;
}

=pod

=encoding utf8

=head1 NAME

ShellWatch::RRD - Interface for shellwatch for receive RRD data and build graphs under RRD::Simple rules. 

=head1 DESCRIPTION

This module receives RRD data sent by some client agent and transform this data in graphs as bellow:

=over

=item * host - name of the host that is the origin of metric data.

=item * metric - name of the metric. Ex: loadavg, meminfo, etc.

=item * time - epoch time that event occurrs

=item * data vars - depends of metric, ex: for loadavg metric, there is a var that calls 'cpuload'.

=back


The graphs originally are built under RRD::Simple rules in the server side.




=head1 SYNOPSIS

  #### on some script.pl
  use common::sense;
  use ShellWatch::RRA;

  my $obj = ShellWatch::RRA->new_with_traits(
    traits => ['loadavg'],
    #mandatory vars(any metric)
    host    => 'some_host',
    time    => $tme_data_from_some_client_host,
    
    #specific metric(loadavg) vars
    cpuload => 102939302, 
    
    #optional vars
    logger => $some_log4perl_obj,
  );

  ### on metric trait
  package ShellWatch::RRA::loadavg;
  use Moose::Role;

  has metric_name => ( is => 'ro', 
                       isa => 'Str', 
                       default => 'loadavg' );
  
  has metric_description => (
      is  => 'ro',
      isa => 'Str',
      default =>
  'Loadaverage is the system load, which is a measure of the amount of computational work that a computer system performs.'
  );

  has vars => (is => 'ro',
               default => sub{['cpuload','avg1', 'avg5' ,'avg15']} ,
               );

  has [qw(cpuload avg1 avg5 avg15)] => (
      is       => 'rw',
      isa      => 'Num',
      required => 0, 
  );


  has rrd_file => (
      is      => 'ro',
      isa     => 'Str',
      lazy    => 1,
      default => sub { join( '/', shift->rrd_directory, 'loadavg.rrd' ) }
  );

  sub update {
      my $self = shift;
      $self->rrd->create( $self->rrd_file, 'avg1' => 'GAUGE', 
                                           'avg5' => 'GAUGE',
                                           'avg15' => 'GAUGE',
      )
        unless -f $self->rrd_file;
      $self->rrd->update( $self->rrd_file, time,
          'avg1' => $self->avg1,
          'avg5' => $self->avg5,
          'avg15' => $self->avg15,
          );
  }

  sub graph {
      my $self = shift;

      my %rtn = $self->rrd->graph(
          height           => $self->graph_height,
          width            => $self->graph_width,
          destination      => $self->graph_directory,
          title            => 'Load Average',
          line_thickness   => 2,
          extended_legend  => 1,
          vertical_label   => 'System load',
          sources          => [qw(avg1 avg5 avg15)],
          #source_colors    => [qw(ff0000)],
          source_drawtypes => [qw(AREA AREA AREA)],
          'units-exponent' => '0',
          rigid => "",
          'slope-mode' => '',
          'font-render-mode' => 'light',
          color            => [
              (
                  "BACK#2F2F2F",   "CANVAS#333333",
                  "SHADEA#CCCCCC", "SHADEB#CCCCCC",
                  "FONT#FFFFFF",   "AXIS#FFFFFF",
                  "ARROW#FF0000",  "GRID#CCCCCC",
                  "MGRID#CCCCCC"
              )
          ],
      );
  }

  1;
  



=head1 ATTRIBUTES


=head2 host

name of the origin data host

=head2 api_time

epoch time of the moment the data was posted

=head2 rrd_directory

path for rrd files

=head2 graph_directory

path for graphs

=head2 hook_directory

path for the hooks

=head2 rrd

RRD::Simple instance.

=head2 graph_width

Graph width(in pixels)

=head2 graph_height

Graph height(in pixels)


=head1 METHODS

=head2 rrd_time 

Returns 'time' Perl core function result.

=head2 get_RRD_data 

Try to get rrd database vars and values through RRD::Simple interface.
Receives 


=head1 AUTHOR

Thiago Rondom L<thiago@eokoe.com>

=head1 MAINTAINER

Andre Carneiro L<andre.carneiro@eokoe.com>

=head1 LICENSE

Copyright 2015 Eokoe


=cut

1;

