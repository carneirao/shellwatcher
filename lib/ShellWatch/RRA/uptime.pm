
package ShellWatch::RRA::uptime;

use Moose::Role;

has metric_name => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'Str',
    default => sub { 'uptime-' . shift->service }
);

has metric_description => (
    is      => 'ro',
    isa     => 'Str',
    default => 'Uptime is a metric that represents the percentage of time that service is successfully operational. It refers to when a system is working.'
);

has service => (
    is       => 'rw',
    isa      => 'Str',
);

has up => (
    is       => 'rw',
    isa      => 'Bool',
);

has vars => (is => 'ro',
             default => sub{return ['up']} ,
             );



sub rrd_file {
    my $self = shift;
    join( '/',
        $self->rrd_directory, join( '', 'uptime-', $self->service, '.rrd' ) );
}

sub update {
    my $self = shift;

    $self->rrd->create(
        $self->rrd_file,
        up => 'GAUGE',
    ) unless -f $self->rrd_file;

    $self->rrd->update(
        $self->rrd_file, time,
        up => $self->up ? 1 : '0',
    );
}

sub graph {
    my $self = shift;

    $self->update unless -f $self->rrd_file;

    $self->rrd->graph(
        $self->rrd_file,
        height         => $self->graph_height,
        width          => $self->graph_width,
        destination    => $self->graph_directory,
        vertical_label => 'UPTIME ' . $self->service,

        extended_legend  => 1,
        line_thickness   => 2,
        rigid => "",
        'slope-mode' => '',
        'font-render-mode' => 'light',

        color => [
            (
                "BACK#2F2F2F",   "CANVAS#333333",
                "SHADEA#CCCCCC", "SHADEB#CCCCCC",
                "FONT#FFFFFF",   "AXIS#FFFFFF",
                "ARROW#FF0000",  "GRID#CCCCCC",
                "MGRID#CCCCCC"
            )
        ],

#        sources          => [qw(up)],
#        source_colors    => [qw(00bbee)],
#        source_drawtypes => [qw(AREA)],

        'CDEF:A=up,UN,0,up,0,GT,IF' => '',
        'VDEF:vA=A,AVERAGE' => '',
        'AREA:A#00bbee:System Availability' => '',
        'GPRINT:vA:= %0.7lf \n' => ''

    );

}

1;

