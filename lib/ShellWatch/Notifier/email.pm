use common::sense;
package ShellWatch::Notifier::email;
use Moose::Role;
use FindBin qw/$Bin/;
use Data::Dumper;
#use Mail::Sendmail;
use Email::Find;
use Email::Sender::Transport::SMTP::TLS;
use Email::Sender::Simple qw(sendmail);
use Email::Simple;
use JSON qw/encode_json decode_json/;
use Config::General qw/ParseConfig/;

has from => ( is => 'rw');

has to => (is => 'rw');

has default_message => (is => 'ro',
                        default => sub { my ($self,) = @_; 
                                         my $host = $self->host;
                                         return q{This is a ShellWatch notification originated from the host '$host'.};
                                        },
                      );

has body_template => ( is => 'ro' ,
                    default => sub{
                                  "HOST: %s\n"
                                  ."METRIC: %s\n"
                                  ."STATUS: %s\n"
                                  ."MESSAGE: %s\n";
                    },
                  );

has type => (is => 'ro',
             default => 'email'
             );


has smtp => ( is => 'rw',
             );

has logger => ( is => 'rw', );


#decoding JSON if exists...
before _validate_data => sub {
  my ($self,) = @_;
  #Default is get data from config. If exists something in json(normally this can came from webapp), 
  #overwrite data with json content
  my ($ok,$data) = (0,$self->load_config_from_metric);
  eval{$data = decode_json($self->json);};
  if($@){
    say "PROBLEMS WITH JSON: $@";
    $self->error("Problems decoding JSON - $@");
  }
  else {
    $self->data($data);
  }
};


#get smtp config
before send_mail => sub {
  my ($self,) = @_;
  my $filepath= join '/', $Bin, '../etc','notifiers.conf';
  if(!-e $filepath){
    say "Can't find notifiers.conf";    
  }
  else {
    my %config = ParseConfig(-ConfigFile => $filepath,);
    my %c = %{$config{engine_conf}->{email}};
    $self->smtp( \%c );
  }
};


sub _validate_data {
  my ($self) = @_;
  my ($ok,$data) = (0,$self->data);
  my $ndata;
  $self->error(''); #avoiding 'noise'...
  foreach my $level(qw/CRITICAL WARNING UNKNOWN/){
    $ndata->{$level} = $data->{$level}->{$self->type};
    if( defined($ndata->{$level}->{responsability}) && $ndata->{$level}->{responsability} ){
      #validating email or list of emails
      my $raw = $ndata->{$level}->{responsability};
      my $group = $1 if $raw =~ /\$(.*)$/;
      if(defined($group) && $group){
        $raw = $self->full_config->{DefaultConfig}->{$group};
      } 
      my @Email_list = ();
      if( $raw =~ /,|;/ ){
        @Email_list = split /,|;/,$raw;
      }
      else {
        @Email_list = ($raw);
      }
      my @Result_list = ();
      my $finder = Email::Find->new(sub {
                                      my($email,$origin_email) = @_;
                                      push @Result_list,$email->format;#just discard a invalid address
                                    },
      );
      $finder->find(\$raw);
      
      if(!@Result_list){
        $self->error(qq/There is no valid e-mail or group on responsability field(level: $level)/);
      }
      elsif(@Result_list != @Email_list){
        $self->error('There is one or more invalid e-mails on responsability field');
      }
      else {
        my ($notification_interval,$first_delay) = ($ndata->{$level}->{interval},$ndata->{$level}->{firstdelay});
        if( $notification_interval !~ /^\d+$/ ){
          $self->error("Field 'notification interval' at level '$level' is invalid!");
        }
        else {
          $ok = 1;
        }
      }
    }
    else {
      $self->error(qq{Field 'responsability' at level '$level' is invalid!});
      $ok = 0;
    }
    last if !$ok;
  }
  return $ok;
}



sub notify {
  my ($self,) = @_;
  my $ok = 0;
  my ($host,$metric,$hook) = ($self->host,
                        $self->metric,
                        $self->name,
                        );
  #Checking elements of notification
  my ($name,$status,$message) = ( $self->name,
                                  $self->status || 'UNKNOWN',
                                  $self->message ,
                                  );

  if(!defined($message) || !$message){
    $message = sprintf($self->body_template,$host,$metric,$status,$self->default_message);
  }
  else {
    $message = sprintf($self->body_template,$host,$metric,$status,$message);
  }

  if(!defined($message) || !$message ){
    $self->error("No message to notify!");
  }
  else {

    #Retrieve conf from status(CRITICAL WARNING etc)
    my $config = $self->config->{$self->status};

    #Notify
    say "NOTIFYING... ". $self->status;
    my $to;
    if(ref($config->{responsability}) =~ /ARRAY/){
      $to = join ',',@{$config->{responsability}} ;
    }
    else {
      $to = $config->{responsability};
    }
   say "To: $to"; 
    my %mail = (  
                header => [
                To      => $to,
                From    => 'shellwatch-notifier@eokoe.com',
                Subject => "$host-$metric notifier - $status",
                ],
                body => "$message\n",
              );
    
    $self->send_mail(\%mail,$config->{'interval'} || $config->{'notification_interval'});
    $ok = 1;
  }
  return $ok;
}

sub send_mail {
  my ($self,$mail,$interval) = @_;
  my $pid = fork();
  die "cannot fork" unless defined $pid;
  if ($pid == 0) {
    exit 0;
  } 
  else {
    local $| = 1;
    my %smtp = %{$self->smtp};
    my $transport = Email::Sender::Transport::SMTP::TLS->new(
         helo     => $smtp{helo},#"b-datum.com",
         host     => $smtp{host},#'smtp.sendgrid.net',
         timeout  => $smtp{timeout},#20,
         port     => $smtp{port},#587,
         username => $smtp{username},#'eokoe',
         password => $smtp{password},#'wae491uyPn', 
     ); 
 
    my $email = Email::Simple->create( %{$mail} );
    my %mail = %{$mail};
    say "Sleeping $interval sec...";
    sleep $interval if defined $interval and $interval > 0;
    sendmail($email, { transport => $transport }) or die "PROBLEMS TO SEND EMAIL!" or die $Email::Sender::Failure::message; 
  }
}


=pod

=encoding=utf8


=head1 NAME

ShellWatch::Notifier::email - support to send notification by e-mail(MooseX::Trait class).

=head1 DESCRIPTION

The main go of this module is treat notification data fields and send it through a e-mail interface

=head1 SYNOPSIS

 use ShellWatch::Notifier;
 
 my $n = ShellWatch::Notifier->new_with_traits(traits => ['email'],
                                                host => 'somehost.somedomain',
                                                metric => 'some_metric',
                                                status => 'CRITICAL', #can be CRITICAL, WARNING or UNKNOWN
                                                message => 'some_message',
                                                );
 $n->notify();


=head1 ATTRIBUTES

=head2 from

One or more recipients for e-mail(separated by comma).

=head2 to

E-mail sender address.

=head2 default_message

if message doesn't exists(because is optional), default message will integrate the notification. 

=head2 body_template

Is a string that follows 'sprintf' template engine. See 'sprintf' documentation typing 'perldoc -f sprintf'

=head2 type

It's a 'read only' attribute to facilitate trait identification. In this case the value is always 'email'.

=head1 METHODS

=head2 _validate_data

Check all notification data patterns. If it's ok, returns true(1). Otherwise returns false(0).

=head2 notify

Prepares the elements of notification and applies all itens into the template. after that calls
'send_mail' method. If it succeeds, it will return true(1). Otherwise returns false(0).

=head2 send_mail

Try to send an e-mail with pretreated notification data. It's done in a forked process.
Returns nothing.

=head1 AUTHOR

Andre Carneiro L<andre.carneiro@eokoe.com>

=head1 LICENSE

Copyright 2015 - Eokoe

=cut



1;
