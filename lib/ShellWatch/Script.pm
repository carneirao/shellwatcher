package ShellWatch::Script;
use Moose;
use namespace::autoclean;
use common::sense;
use File::Path qw/make_path/;
use Capture::Tiny ':all';
use Data::Dumper;
use Carp qw/confess/;
use FindBin qw/$Bin/;

our $VERSION = '0.1.8';

has host => (
  is       => 'ro',
  isa      => 'Str',
  required => 1,
  trigger  => sub {
    my ( $self, $value ) = @_;
    $self->error("Invalid host! '$value'") if !defined $value or !$value;
  },
);

has metric => (
  is       => 'ro',
  isa      => 'Str',
  required => 1,
  trigger  => sub {
    my ( $self, $value ) = @_;
    $self->error("Invalid metric! '$value'") if !defined $value or !$value;
  },
);

has code => (
  is       => 'rw',
  isa      => 'Str',
);

has period => (
  is      => 'rw',
  isa     => 'Int',
  default => '10',    #seconds
);

has last_run => ( is => 'rw' );

has env => (
  is  => 'rw',
  isa => 'HashRef',
);

has error => (
  is      => 'rw',
  default => sub { return '' }
);

has base_dir => (
  is      => 'rw',
  default => sub {
    return join( '/', "$Bin/../", 'hooks' );
  },
);

has link_dir => (
  is      => 'rw',
  default => sub {
    return join( '/', "$Bin/../", 'meta' );
  },
);

has identifier => (
  is       => 'ro',
  isa      => 'Str',
  required => 1,
  trigger  => sub {
    my ( $self, $value ) = @_;
    $self->error("Invalid identifier!") if !defined $value or !$value;
  },
);


has logger => ( 
  is => 'rw',
  );

has user => (
  is => 'ro',
  required => 1,
);


has script_selected => (
  is => 'rw',
  isa => 'Str',
  default => '', 
);

sub replace_code {
  my ($self,) = @_;
  say "REPLACING CODE...";
  my $ok = 0;
  my $file = join '/',$self->base_dir,$self->host,$self->metric,$self->script_selected;
  if(!-e $file){
    my $msg = "Script file '$file' was not found!";
    $self->error($msg);
    $self->logger->error($msg . " - $!");
  }
  elsif(!open my $fh,'<:encoding(UTF-8)',$file) {
    my $msg = "Can't open file '$file'"; 
    $self->error($msg);
  }
  else {
    my $code = '';
    while(<$fh>){
      $code .= $_;
    }
    say "REPLACE CODE: $code";
    $self->code($code);
    $ok = 1 if length $code > 0;
    undef $code;
  }
  return $ok;
}

sub add {
  my ( $self  ) = @_;
  my $ok    = 0;
  my $error = '';
  
  if ( !$self->error ) {    
    my ( $host, $identifier,$user ) = ( $self->host, $self->identifier,$self->user );
    my $host_dir = join '/', $self->base_dir, $host, $self->metric ;
    map{ make_path($_) if !-d $_; }( $host_dir ); 
    my $filepath = join( '/' , $host_dir, $identifier) ;
    #checking if script exists
    print STDERR "\n\nFUCKING FILEPATH: $filepath\.sh";
    if(-e $filepath . '.sh'){
      $error = "This script already exists!";
    }
    else {
      foreach my $ext('.sh','.period'){
        $ok = 1;
        print STDERR "\n\nFUCKING FILEPATH: $filepath$ext";
        if(!open my $fh,'>encoding(UTF-8)',$filepath . $ext ){
          $error = "Não foi possível salvar o script '$filepath' - $!";
          $self->logger->error($error . " - $!");
          $ok = 0;
          last;
        }
        else {
          if($ext eq '.sh'){
            $self->replace_code if $self->script_selected;
            print $fh $self->code;
          }
          else {
            print $fh $self->period;
          }
          close $fh if defined $fh;
          if(!chmod 0755,$filepath . $ext){
            $error = 'Problems to change permission!'; 
          }
          else {
            $ok = 1;
          }
        }
      }
      if(!$ok || !$self->_make_user_links($filepath) ){
        $error = "Problems to create links!";
      }
    }
    $self->error($error);
  }
say STDERR "\nDEBUG1: $ok " . $error ;
  return $ok;
}



sub _make_user_links {
  my ($self, $realfile ) = @_;
  my $ok = 0;
  my $error = '';
  my @Link_paths = ( join( '/', $self->link_dir, $self->host, $self->user, $self->metric),
                    join( '/', $self->base_dir, $self->host, $self->user, $self->metric),
  );
  foreach my $path(@Link_paths){
    make_path $path if !-d $path;
    foreach my $ext('.sh','.period'){
      my $origin = $realfile . $ext;
      my $destiny = join  '/', $path, $self->identifier . $ext ;
      my $r = '';
      eval{$r = system "ln -s $origin $destiny";};
      if(!-e $destiny){
        $error = "Error when trying to create a link ";
        $self->logger->error($error . " [$destiny - $!]");
      }
      else {
        $self->logger->info("Link '$destiny' created!");
        $ok = 1;
      }
    }
  }
  $self->error($error) if $error;
  return $ok;
}



sub remove {
  my ( $self, ) = @_;
  my $ok = 0;
  my $error = '';
  my ( $host, $identifier ) = ( $self->host, $self->identifier );
  my $host_dir = join '/', $self->base_dir, $host, $self->metric ;
  
  my @Link_paths = ( join( '/', $self->link_dir, $self->host, $self->user, $self->metric),
                    join( '/', $self->base_dir, $self->host, $self->user, $self->metric),
  );
  $identifier =~ s/\.(sh|period)$//;
  #Removing links 
  foreach my $path(@Link_paths){
    my $target = join '/', $path, $identifier . '.*';
    if(!unlink glob $target){
      $error = "Can't remove script link";
      say $error . " - $!";
      $self->logger->error($error . " - '$target.*' - $!");
      last;
    }
    else {
      $self->logger->info("Link '$target' removed!");
      $ok = 1;
    }
  }

  #Remove file
  if($ok){
    $ok = 0;
    my $target = join '/', $host_dir, $identifier . '.*';
    if(!unlink glob $target){
      $error = "Can't remove script";
      say $error . " - $!";
      $self->logger->error($error . " - '$target.*' - $!");
      last;
    }
    else {
      $self->logger->info("File '$target' removed!");
      $ok = 1;
    }
  }

  
  return $ok;
}


__PACKAGE__->meta->make_immutable;


=pod

=encoding utf-8

=head1 NAME

ShellWatch::Script - Hook script abstraction and CRUD

=head1 DESCRIPTION

This module is a abstraction for hook scripts. It means that is possible to create and delete script hooks
through this module.

=head1 SYNOPSIS

 

=head1 ATTRIBUTES


=head2 host 

String that must contain the host name

=head2 metric

String that must contain the metric name

=head2 period

Integer that must contain the amount in seconds of interval that the script will be run.

=head2 code

String that must contain the bash code for the hook script

=head2 identifer

Is the name of the hook script.

=head2 base_dir

String that must contain the base dir of all hooks scripts. By default is <SHELLWATCH_PATH>/hooks

=head2 link_dir

String that must contain the base dir of all hooks scripts. By default is <SHELLWATCH_PATH>/hooks

=head1 METHODS

=head2 add()

Saves script creating a real file in <SHELLWATCH_HOME>/hooks/<HOST>/<METRIC> and creates links in user's dirs in meta and hooks directory. If success, return true(1).
Otherwise returns false(2).

=head2 remove()

Try to remove a new script hook file and all links. If succeeded returns true(1). Otherwise returns false(0).


=cut

1;
